//
//  ViewController.m
//  WJInternational
//
//  Created by 文杰 许                       on 2018/6/29.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import "ViewController.h"

#define kLocal(key) NSLocalizedString(key, nil)
#define kLocalTable(key, tbl) NSLocalizedStringFromTable(key, tbl, nil)

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImageView *imageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:kLocal(@"ic_sel_grade_all")]];
    imageV.frame = CGRectMake(self.view.bounds.size.width / 2 - 50, 100, 100, 100);
    [self.view addSubview:imageV];
   
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 50)];
    title.text = kLocal(@"title");
    title.textAlignment = NSTextAlignmentCenter;
    title.center = self.view.center;
    [self.view addSubview:title];
    
    UILabel *detail = [[UILabel alloc] initWithFrame:CGRectMake(self.view.bounds.size.width / 2 - 50, CGRectGetMaxY(title.frame) + 20, 100, 50)];
    detail.text = kLocalTable(@"detail", @"WJLocal");
    detail.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:detail];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
