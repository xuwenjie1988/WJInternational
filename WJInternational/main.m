//
//  main.m
//  WJInternational
//
//  Created by 文杰 许                       on 2018/6/29.
//  Copyright © 2018年 文杰 许                      . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
